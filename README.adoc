= Documentation Repository

This repository contains documentation and it is basically an awesome lists with some extended articles
The main topic treated are the following

- link:docs/space-science/README.adoc[Space Science]
- link:docs/computer-science/README.adoc[Computer Science]
- link:docs/science/physics.adoc[Physics]
- link:docs/game-development/README.adoc[Game Development]
- link:docs/gentoo/README.adoc[Gentoo]

== About it ==

== AsciiDoc ==

So far the markup language selected for the content has been Asciidoc

- https://asciidoctor.org/docs/asciidoc-writers-guide/[How to write]
- https://asciidoctor.org/docs/asciidoc-syntax-quick-reference/[Quick Reference]
- http://asciidoc.org/index.html[AsciiDoc.org]