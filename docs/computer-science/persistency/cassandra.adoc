= Cassandra
:source-highligther: highlight.js

Some video points out that Cassandra is the holy grail of distributed databases.
I do not have a first hand experience with it and my other distributed databases experiences are related to
single node SQL Databases and Distributed HashMaps with Hazelcast and just HashMaps with Redis.

a

= As a NO-SQL

Cassandra is part of the Apache Foundation and should be behaving like a Consistent Hashing Distributed HashMap.
As a distributed HashMap it adds distribution to other already existing NOSQL.

It has a lot of client-api and as so it is a great solution to provide a distributed hash map.



== Example: Persisting thousands of links

A javascript app (client-server) allow a user to keep a database of urls.
Cassandra is used to provide the persistence

=== Server

=== Client

=== Docker Cassandra

Starting the first node

[source,language=shell,linenumbers]
----
docker pull cassandra

docker run --name some-cassandra --network some-network -d cassandra:tag
docker run --name some-cassandra2 --network some-network -e CASSANDRA_SEEDS=some-cassandra -d cassandra:tag
----

== Example: Cassandra as an event store


[source,language=bash,linenumbers]


=== Links

- https://hub.docker.com/_/cassandra/[Docker Image]
- https://stackoverflow.com/questions/19321682/using-cassandra-as-an-event-store[Cassandra as event store]
- https://cassandra.apache.org/download/[Cassandra main site]
