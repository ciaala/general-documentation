= Database scaling the log =

== Performance bottleneck ==

The most efficient architecture to execute the Write operation is the insertion at the end of a LinkedList

Each node of the linked list contain an insert/update/delete of a record of a table

But LinkedList are not efficient to perform Read because to find a record the read algorithm have to look at all the elements
of the linked list.

[%header,cols=3*]
|===
| Operation | LinkedList | SortedArray

| Read | O(n) | O(log(n))

| Write | O(1) | O(n log(n))
|===

== Improving write performance ==

LinkedList is the best approach to make the Write fast

You then want to do batch the write request when sending them.

That database has received **k** new records/updates and the database's engine
has to decide on how to to serve the data on Read has to either
create a new SorterArray by sort everything O( (n + k) log(n+k) ) or keep the partial
structure and do read operation on both the previous SortedArray of size n
and then search in the smaller batch of size k.
Bloom filtering can be used to improve the performance of the search by paying a small amount of memory.

== Bloom Filter ==

Is a technique to provide a fast lookup to verify that the value associated with the key is
actually present in the block.
The bloom filter acts like hashset but that use a key of fixed length, always the first bite/bites of the original key.
for example considering a list of strings, a bloom filter of the two first characters would be size 1 bit for each element.
2 bytes are equals to 256 elements * 256 elements = 64 k elements => 64k bits -> 8 KB of memory (minimum).

Of course the first 2 characters do not represent the entire string but are a great way to verify if a set of small set of K elements
does not contain the key.

Of course in case of a dictionary of words of the english language you can use less than 8 bits to represent a valid character.
Something around the 26 characters -> 32 elements `2^5` or even 64 `2^6` could be enough to represent all the strings.
```
2^5 * 2^5 = 2^10
          = 1024
          = 1k bit
          = 128 bytes
```

=== Example ===
[source,C++]
----

// Batch size
auto Keys = ['abc', 'abd', 'def','hgf', 'ard', 'zxc'];
auto Values = [12, 34, 56, 78, 90, 00 ];

// Bloom filter
//
auto bloom = [
                false, // aa
                true, // ab
                false, // ac
                ...
                true, // ar
                ...
                true, // de
                ...
                true, // hg
                ...
                true, // zx
];

// Search Key
auto lookup_key = 'abd';

auto bloom key = loopup_key.substring(0,2) ;

blloom
----





== Sorted Stream Table ==

It is anyway a clear winning scenario to merge the temporary batch
of data with the pre-existing, and already sorted, full data-set.

It is convenient to do so as soon as possible but it can be done in a seperate thread.
The merge operation can be made very efficiently if you we consider that we are in the
condition of the merge step in a merge-sort algorithm.
The two collections to merge are already sorted and unifying them require to copy both
elements in the final collection; a total of `O(n+k)` operations.
