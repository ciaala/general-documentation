= GODOT

== Platform supported

Godot engine supports all the relevant platforms

- Mobiles IOS and Android
- Desktop
    - macOS
    - Windows
    - Linux
        - X11
    - BSD
        - X11
- Web
    - https://emscripten.org/docs/introducing_emscripten/index.html[emscripten.org]
        - asm.js and WebAssembly, built using LLVM