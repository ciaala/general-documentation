= Family Portrait (of the Solar System)

In 2020 I continued to dig deep in Astronomy, Spacecraft, Space Exploration History
and I ended up coming up on the picture of the Portrait of the Solar System.
The picture was taken by the *Vojager 1*
https://en.wikipedia.org/wiki/Family_Portrait_(Voyager)[Wikipedia]

Apparently another spacecraft *Messenger* made another picture too in 2010,
without improving on the quality of the details captured.

Family Portrait is also the famous picture of _Pale Blue Dot_ by Carl Segan
https://en.wikipedia.org/wiki/Pale_Blue_Dot[Wikipedia]




== Note
_This reminds me that I should probably find the picture of Neptune by Vojager 2_

_And collect also the first picture of Mars Path Finder of 1997_
