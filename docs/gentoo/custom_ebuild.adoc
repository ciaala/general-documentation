= Customise an existing Ebuild
:source-highlighter: rouge
:icons: font

== Add a Ruby Target

.Updating the file
[source,bash,linenum]
----
# If not running interactively, don't do anything
[[ -z "$PS1" ]] && return
mkdir -p /var/db/repos/local/app-text/ronn
cd /var/db/repos/local/app-text
cp -r /usr/portage/app-text/ronn .
cd ronn
vim ronn-0.7.3-r3.ebuild
----

.Updating the file
[source,sh,linenum]
----
# If not running interactively, don't do anything
[[ -z "$PS1" ]] && return
mkdir -p /var/db/repos/local/app-text/ronn
cd /var/db/repos/local/app-text
cp -r /usr/portage/app-text/ronn .
cd ronn
vim ronn-0.7.3-r3.ebuild
----

.Updating the file
[source,shell,linenum]
----
# If not running interactively, don't do anything
[[ -z "$PS1" ]] && return
mkdir -p /var/db/repos/local/app-text/ronn
cd /var/db/repos/local/app-text
cp -r /usr/portage/app-text/ronn .
cd ronn
vim ronn-0.7.3-r3.ebuild
----

.Outline in Java
[source,java,linenum]
----
public class pippo {
    public static void main(String [] args) {
        if ( args.length > 2) {
            System.out.println("123",123);
        }
    }
}
----